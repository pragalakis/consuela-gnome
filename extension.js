const St = imports.gi.St;
const Main = imports.ui.main;
const Soup = imports.gi.Soup;
const Mainloop = imports.mainloop;

const url = 'https://www.lambdaspace.gr/hackers.txt';
const EnabledIcon = 'lambda-on';
const DisabledIcon = 'lambda-off';
let container, icon;

function _checkStatus() {
  const _httpSession = new Soup.SessionAsync();
  let request = Soup.Message.new('GET', url);

  _httpSession.queue_message(request, function(_httpSession, message) {
    if (message.status_code !== 200) {
      global.log('Lambda Space status error code ', message.status_code);
      return;
    }

    let status = request.response_body.data;

    if (Number(status)) {
      icon.icon_name = EnabledIcon;
      container.set_child(icon);
    } else {
      icon.icon_name = DisabledIcon;
      container.set_child(icon);
    }
  });
}

function _timer() {
  // Loop time in miliseconds
  Mainloop.timeout_add(600000, () => {
    _checkStatus();
    return true;
  });
}

function init(extensionMeta) {
  // Allow to use resources on /icons path
  let theme = imports.gi.Gtk.IconTheme.get_default();
  theme.append_search_path(extensionMeta.path + '/icons');

  // Create new icon
  icon = new St.Icon({
    icon_name: DisabledIcon,
    style_class: 'system-status-icon'
  });

  // Simple container for one child
  container = new St.Bin({
    style_class: 'panel-button',
    reactive: false,
    can_focus: false,
    x_fill: true,
    y_fill: false,
    track_hover: false
  });

  // Init checkStatus and timer functions
  _checkStatus();
  _timer();

  container.set_child(icon);
}

function enable() {
  Main.panel._rightBox.insert_child_at_index(container, 0);
}

function disable() {
  Main.panel._rightBox.remove_child(container);
}
